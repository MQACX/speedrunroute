﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SpeedrunRoute.Server.Services.Game;
using SpeedrunRoute.Shared.Models;

namespace SpeedrunRoute.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {
        private readonly IGameService _gameService;
        private readonly ILogger<GameController> _logger;

        public GameController(IGameService gameService, ILogger<GameController> logger)
        {
            _gameService = gameService;
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<GameModel> GetAll()
        {
            return _gameService.GetAll();
        }
    }
}
