using System.Collections.Generic;
using SpeedrunRoute.Shared.Models;
using SpeedrunComSharp;
using AutoMapper;

namespace SpeedrunRoute.Server.Services.Game
{
    public class GameService : IGameService
    {
        private readonly IMapper _mapper;

        public GameService(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IEnumerable<GameModel> GetAll()
        {
            var client = new SpeedrunComClient();

            var games = client.Games.GetGames(yearOfRelease: 1997);
            return _mapper.Map<IEnumerable<GameModel>>(games);
        }
    }
}