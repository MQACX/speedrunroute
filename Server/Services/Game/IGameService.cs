using System.Collections.Generic;
using SpeedrunRoute.Shared.Models;

namespace SpeedrunRoute.Server.Services.Game
{
    public interface IGameService
    {
        public IEnumerable<GameModel> GetAll();
    }
}