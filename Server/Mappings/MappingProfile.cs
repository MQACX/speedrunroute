using AutoMapper;
using SpeedrunComSharp;
using SpeedrunRoute.Shared.Models;

namespace SpeedrunRoute.Server.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Game, GameModel>();
        }
    }
}