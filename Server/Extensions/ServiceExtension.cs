﻿using Microsoft.Extensions.DependencyInjection;
using SpeedrunRoute.Server.Services.Game;

namespace SpeedrunRoute.Server.Extensions
{
    public static class ServiceExtension
    {
        public static void AddMultipleScoped(this IServiceCollection services)
        {
            services.AddScoped<IGameService, GameService>();
        }
    }
}
