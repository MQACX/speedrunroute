namespace SpeedrunRoute.Shared.Models
{
    public class GameModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string JapaneseName { get; set; }
        public string Abbreviation { get; set; }
        public int? YearOfRelease { get; set; }
    }
}