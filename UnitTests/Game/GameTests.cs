﻿using System.Linq;
using SpeedrunRoute.Server.Services.Game;
using Xunit;

namespace SpeedrunRoute.UnitTests.Game
{
    public class GameTests
    {
        private readonly IGameService _gameService;

        public GameTests(IGameService gameService)
        {
            _gameService = gameService;
        }

        [Fact]
        public void ShouldGetGames()
        {
            var test = _gameService.GetAll().ToList();
            System.Console.WriteLine(test);
        }
    }
}
